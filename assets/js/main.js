const bulletSize = 10; // px
const bulletSpeed = 5;

function getMousePos(e) {
    if (e instanceof MouseEvent) {
        var x = e.clientX;
        var y = e.clientY;
    } else if (e instanceof TouchEvent) {
        var x = e.touches[0].clientX;
        var y = e.touches[0].clientY;
    }
    return [x, y];
}

function spawnBullets(x, y) {
    var div = document.createElement('div');
    div.style.left = (x) + 'px';
    div.style.top = (y) + 'px';
    div.style.width = bulletSize + 'px';
    div.style.height = bulletSize + 'px';
    div.style.position = 'absolute';
    div.classList.add('bullet');
    var directions = ['up', 'right', 'down', 'left'];
    var colors = ['#FF0000', '#00FF00', '#00FFFF', '#FFFF00', '#FF00FF', '#9D00FF'];
    for (let i = 0; i < 4; i++){
        var newbullet = div.cloneNode(true);
        newbullet.classList.add(directions[i]);
        randColor = Math.floor(Math.random() * colors.length);
        newbullet.style.backgroundColor = colors[randColor];
        document.getElementById('main').appendChild(newbullet);
    }
}

function updatePos(bullet, vx, vy) {
    var currentX = parseInt(bullet.style.left);
    var currentY = parseInt(bullet.style.top);
    var newX = currentX + vx;
    var newY = currentY + vy;
    var maxX = window.innerWidth;
    var maxY = window.innerHeight;

    if (newX < 0 || newX + bulletSize >= maxX || newY < 0 || newY + bulletSize >= maxY) {
        bullet.remove();
    } else {
        bullet.style.left = (newX) + 'px';
        bullet.style.top = (newY) + 'px';
    }
}

function updateBullets() {
    bulletsup = document.getElementsByClassName('bullet up');
    bulletsright = document.getElementsByClassName('bullet right');
    bulletsdown = document.getElementsByClassName('bullet down');
    bulletsleft = document.getElementsByClassName('bullet left');

    for (let i = 0; i < bulletsup.length; i++) {
        updatePos(bulletsup[i], 0, bulletSpeed);
    }
    for (let i = 0; i < bulletsright.length; i++) {
        updatePos(bulletsright[i], bulletSpeed, 0);
    }
    for (let i = 0; i < bulletsdown.length; i++) {
        updatePos(bulletsdown[i], 0, -bulletSpeed);
    }
    for (let i = 0; i < bulletsleft.length; i++) {
        updatePos(bulletsleft[i], -bulletSpeed, 0);
    }

    requestAnimationFrame(updateBullets);
}

function checkBulletSpawn() {
    if (mouseisdown) {
        spawnBullets(currentMousePos[0], currentMousePos[1]);
    }
}

var currentMousePos;
var mouseisdown = false;  //Global ID of mouse down interval
function mousedown(e) {
    if (!mouseisdown) {
        mouseisdown = true;
    }
    currentMousePos = getMousePos(e);
}
function mouseup() {
    if (mouseisdown) {
        mouseisdown = false;
    }
}

//Assign events
document.addEventListener('mousedown', mousedown);
document.addEventListener('touchstart', mousedown);
document.addEventListener('mouseup', mouseup);
document.addEventListener('touchend', mouseup);
document.addEventListener('mousemove', function(e){currentMousePos = getMousePos(e);})
document.addEventListener('touchmove', function(e){currentMousePos = getMousePos(e);})
//Also clear the interval when user leaves the window with mouse
// document.addEventListener('mouseout', mouseup);

setInterval(checkBulletSpawn, 100);
requestAnimationFrame(updateBullets);
